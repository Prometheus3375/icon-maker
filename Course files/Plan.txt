Plan for project development

Iteration 1 (03.02):
    - Select programming language (C#, Java or Python) based on next criterias:
        - Possibility of generating executable file for Windows and Linux.
        - Image processing libraries which supports mentioned image formats (.jpg, .dds, .bmp, etc.).
        - Graphical user interface libraries with modern graphics.
    - Create public git repository with BSD licence.
    - Create several frames.
    
    Acceptance criteria:
    - Programming language is selected and the decision is justified.
    - Repository is created and contains licence file.
    - Borders are created and uploaded to repository.
Iteration 2 (17.02):
    - Create console application which reads an image, applies frame and saves the result.
    - Sketch future GUI.
    
    Acceptance criteria:
    - Methods for reading and saving images are defined.
    - Algorithm of frame applying is implemented.
    - Size and position of GUI elements are specified.
Iteration 3 (02.03):
    - Create GUI with next elements:
        - list of frames;
        - image previewer;
        - buttons for "read" and "save" options;
        - a button for removing applied frame.
    
    Acceptance criteria:
    - Options "read" and "save" works properly.
    - When an image is read, it should be shown in image previewer.
    - Whenever a user selects a frame, already selected frame should be removed, the new one is applied. The result should be shown in image previewer.
Iteration 4 (16.03):
    - Change behaviour of "Select frame" button.
    - Fix image previewer.
    - Refactor code and minor fixes.
    - Add displaying of image information (size, bits per pixel, format, etc.).

    Acceptance criteria:
    - "Select frame" button toggles view of frames’ window.
    - Images smaller than 300x300 are centered in previewer.
    - Violations of MVVM pattern are removed.
    - User can see image information below the image previewer.
Iteration 5 (30.03):
    - Create settings for frames.
    - Design and develop GUI for settings changing.
    - Sketch window for batch image processing.
    
    Acceptance criteria:
    - User can specify how image is resized, to which point inside frame the image is placed, etc.
    - User can change settings of frames in the interface of the program.
    - Size and position of GUI elements are specified.
Iteration 6 (13.04):
    - Implement batch image processing.
    - Add "About program" window.
    - Fill README.md on repository.
    
    Acceptance criteria:
    - User can specify a path where to get images and a path where to save images.
    - User can select a frame to apply.
    - After user confirmation images with applied frames are saved in specified path.
    - "About program" window contains information about university, supervisor, developer and link to the documentation.
    - README.md contains examples of software usage.

