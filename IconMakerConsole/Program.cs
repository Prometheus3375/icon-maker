using System;
using System.IO;
using System.Linq;
using ImageMagick;
using System.Diagnostics;
using System.Globalization;

namespace IconMakerConsole
{
public static class Program
    {
    public static void MagickNETTests()
        {
        using MagickImage border = new MagickImage("frames/Passive 2.png"),
                          image = new MagickImage("images/Secret Tactics 64x64.png");

        //Console.WriteLine(MagickColors.Transparent.ToString());  // #FFFFFF00

        #region Resizing
        // http://www.imagemagick.org/Usage/resize/
        //image.VirtualPixelMethod = VirtualPixelMethod.Transparent;  // to save trasparancy. UPD: not necesssary
        //image.Resize(64, 64);
        //image.Scale(64, 64);
        //image.Write("result.png", MagickFormat.Png);
        #endregion

        #region Change alpha
        //image.HasAlpha = true;
        //image.CopyPixels(border, Channels.Alpha);
        //image.Write("result.png", MagickFormat.Png);
        #endregion

        #region Merging
        // http://www.imagemagick.org/Usage/compose/
        //image.Composite(border, CompositeOperator.SrcOver);  // Over = SrcOver, means border will be above
        #region For white tranparent pixels
        //var clone = image.Clone();
        //image.ColorAlpha(MagickColors.White);
        //image.HasAlpha = true;
        //image.CopyPixels(clone, Channels.Alpha);
        #endregion
        //image.Write("result.png", MagickFormat.Png);
        #endregion

        #region Merging
        //MagickImageCollection images = new MagickImageCollection();
        //images.Add(image);
        //images.Add(border);
        //var result = images.Merge();
        //result.HasAlpha = false;  // to remove alpha
        //result.Write("result.png", MagickFormat.Png);
        #endregion
        }

    public static void ExceptionTest()
        {
        // ReSharper disable once UnusedParameter.Local
        static void Throw(int i)
            {
            throw new ArgumentException("Exception message", nameof(i));
            }

        Throw(0);
        }

    public static void Main()
        {
        #region CWD settings
        var cwd = new DirectoryInfo(Directory.GetCurrentDirectory());
        // ReSharper disable PossibleNullReferenceException
        for (int i = 0; i < 4; ++i)
            cwd = cwd.Parent;
        Directory.SetCurrentDirectory(cwd.FullName);
        // ReSharper restore PossibleNullReferenceException 
        #endregion

        try
            {
        ExceptionTest();
            }
        catch (Exception e)
            {
            Console.WriteLine($"Exception: {e}");
            Console.WriteLine($"Type: {e.GetType().Name}");
            Console.WriteLine($"Full type: {e.GetType().FullName}");
            Console.WriteLine($"Message: {e.Message}");
            Console.WriteLine("Start of stack trace");
            Console.WriteLine(e.StackTrace);
            Console.WriteLine("End of stack trace");
            Console.WriteLine($"Source: {e.Source}");
            Console.WriteLine($"Target Site: {e.TargetSite}");
            }
        }
    }
}
