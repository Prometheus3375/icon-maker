using System;
using System.Globalization;
using System.Text.RegularExpressions;
using ImageMagick;

namespace IconMakerCore.Common
{
public static class ColorParser
    {
    public static class Patterns
        {
        private const string symbols = @"#[a-fA-F0-9]";
        public static readonly Regex AC = new Regex($@"^{symbols}{{2}}$", RegexOptions.Compiled | RegexOptions.Singleline);
        public static readonly Regex RGB = new Regex($@"^{symbols}{{3}}$", RegexOptions.Compiled | RegexOptions.Singleline);
        public static readonly Regex ARGB = new Regex($@"^{symbols}{{4}}$", RegexOptions.Compiled | RegexOptions.Singleline);
        public static readonly Regex RRGGBB = new Regex($@"^{symbols}{{6}}$", RegexOptions.Compiled | RegexOptions.Singleline);
        public static readonly Regex AARRGGBB = new Regex($@"^{symbols}{{8}}$", RegexOptions.Compiled | RegexOptions.Singleline);
        }

    /// <summary>
    ///     Tries to extract from a hex string color values
    /// </summary>
    /// <param name="colorString">
    ///     A string to parse. The following patterns are acceptable:
    ///     <list type="table">
    ///         <item>
    ///             #ac -> #aacccccc
    ///         </item>
    ///         <item>
    ///             #rgb -> #FFrrggbb
    ///         </item>
    ///         <item>
    ///             #argb -> #aarrggbb
    ///         </item>
    ///         <item>
    ///             #rrggbb -> #FFrrggbb
    ///         </item>
    ///         <item>
    ///             #aarrggbb
    ///         </item>
    ///     </list>
    ///     r, g, b, c represent a hex digit, F is F in hex.
    ///     If the string does not match any pattern then <see langword="false" /> is returned and <paramref name="color" /> is not modified.
    /// </param>
    /// <param name="color">
    ///     Color object which will contain parsed values on success
    /// </param>
    /// <returns>
    ///     <see langword="true" /> if <paramref name="colorString" /> matches specified patterns, <see langword="false" /> otherwise
    /// </returns>
    public static bool TryParse(string colorString, MagickColor color)
        {
        byte a, r, g, b;
        if (Patterns.AC.IsMatch(colorString))
            {
            a = Byte.Parse($"{colorString[1]}{colorString[1]}", NumberStyles.HexNumber);
            r = Byte.Parse($"{colorString[2]}{colorString[2]}", NumberStyles.HexNumber);
            g = r;
            b = r;
            }
        else if (Patterns.RGB.IsMatch(colorString))
            {
            a = 255;
            r = Byte.Parse($"{colorString[1]}{colorString[1]}", NumberStyles.HexNumber);
            g = Byte.Parse($"{colorString[2]}{colorString[2]}", NumberStyles.HexNumber);
            b = Byte.Parse($"{colorString[3]}{colorString[3]}", NumberStyles.HexNumber);
            }
        else if (Patterns.ARGB.IsMatch(colorString))
            {
            a = Byte.Parse($"{colorString[1]}{colorString[1]}", NumberStyles.HexNumber);
            r = Byte.Parse($"{colorString[2]}{colorString[2]}", NumberStyles.HexNumber);
            g = Byte.Parse($"{colorString[3]}{colorString[3]}", NumberStyles.HexNumber);
            b = Byte.Parse($"{colorString[4]}{colorString[4]}", NumberStyles.HexNumber);
            }
        else if (Patterns.RRGGBB.IsMatch(colorString))
            {
            a = 255;
            r = Byte.Parse(colorString[1..3], NumberStyles.HexNumber);
            g = Byte.Parse(colorString[3..5], NumberStyles.HexNumber);
            b = Byte.Parse(colorString[5..7], NumberStyles.HexNumber);
            }
        else if (Patterns.AARRGGBB.IsMatch(colorString))
            {
            a = Byte.Parse(colorString[1..3], NumberStyles.HexNumber);
            r = Byte.Parse(colorString[3..5], NumberStyles.HexNumber);
            g = Byte.Parse(colorString[5..7], NumberStyles.HexNumber);
            b = Byte.Parse(colorString[7..9], NumberStyles.HexNumber);
            }
        else
            return false;

        color.A = a;
        color.R = r;
        color.G = g;
        color.B = b;
        return true;
        }
    }
}
