using System;
using System.Collections.Generic;
using System.Reflection.Metadata;
using System.Text;

namespace IconMakerCore.Common
{
public static class Errors
    {
    public static readonly List<Error> List = new List<Error>();

    public sealed class Error
        {
        public string Description { get; }
        public string Message { get; }

        public Error(string description, Exception e)
            {
            Description = description;
            Message = e.Message;
            }
        }

    public static void Save(Exception e, string description = null)
        {
        description ??= e.GetType().FullName;
        List.Add(new Error(description, e));
        }

    public static Error[] CopyAndClear()
        {
        var result = List.ToArray();
        List.Clear();
        return result;
        }
    }
}
