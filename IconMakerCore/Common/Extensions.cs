using System;
using System.Collections.Generic;
using ImageMagick;

namespace IconMakerCore.Common
{
public static class Extensions
    {
    public static string ToARGBString(this MagickColor color)
        {
        var colorString = color.ToString();
        return $"#{colorString[7..9]}{colorString[1..7]}";
        }

    public static string SizeInfo(this IMagickImage image)
        {
        return $"{image.Width}x{image.Height}x{image.Depth * image.ChannelCount}";
        }

    public static TEnum[] EnumGetUniqueValues<TEnum>() where TEnum: struct
        {
        var list = new List<TEnum>();
        foreach (TEnum v in Enum.GetValues(typeof(TEnum)))
            {
            if (list.Count == 0 || !list[^1].Equals(v))
                list.Add(v);
            }
        return list.ToArray();
        }
    }
}
