using System;
using System.IO;

namespace IconMakerCore.Common
{
public static class IO
    {
    public static bool CreateDirectory(string path, bool deleteIfFile = true)
        {
        if (Directory.Exists(path))
            return true;
        try
            {
            if (File.Exists(path))
                {
                if (deleteIfFile)
                    File.Delete(path);
                else
                    return false;
                }
            Directory.CreateDirectory(path);
            return true;
            }
        catch (SystemException /*e*/)
            {
            // Log error
            return false;
            }
        }
    }
}
