using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace IconMakerCore.Common
{
public static class Logger
    {
    public const string LogDirectory = "logs";
    public static readonly string LogFilePath = Path.Combine(LogDirectory, $"{DateTime.UtcNow:yyyy-MM-dd HH-mm}.txt");

    public static readonly Encoding FileEncoding = new UTF8Encoding(false, false);

    public static void Log(string message)
        {
        try
            {
            File.AppendAllText(LogFilePath, $"{DateTime.UtcNow:yyyy-MM-dd HH:mm:ss.fff} -> {message}\n", FileEncoding);
            }
        catch (Exception e)
            {
            Errors.Save(e, "An error occured while adding a new entry to the log file");
            }
        }

    public static void LogError(Exception e, string description = null)
        {
        description ??= e.GetType().FullName;
        Log($"{description}: {e.Message} {{\n{e.StackTrace}}}");
        }
    }
}
