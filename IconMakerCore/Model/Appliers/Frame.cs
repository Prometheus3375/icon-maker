using System.Diagnostics;
using IconMakerCore.Model.Settings.Appliers;
using ImageMagick;

namespace IconMakerCore.Model.Appliers
{
public sealed class Frame: IApplier
    {
    public string Path { get; }
    public string Name { get; }

    private IMagickImage ApplyingImage { get; }
    public ApplierSettings Settings { get; set; }

    public int Width => ApplyingImage.Width;
    public int Height => ApplyingImage.Height;

    public Frame(string path, IMagickImage image)
        {
        Path = path;
        ApplyingImage = image;
        Name = System.IO.Path.GetFileNameWithoutExtension(path);
        }

    public IMagickImage Apply(IMagickImage image)
        {
        //Debug.WriteLine($"Filter used {image.FilterType}");
        Debug.WriteLine($"Resize mode - {Settings.ResizeMode}");

        var geometry = Settings.ResizeGeometry;

        if (Settings.ResizeMode == ResizeMode.ApplierToImage)
            {
            using var applying = ApplyingImage.Clone();
            geometry.Width = image.Width;
            geometry.Height = image.Height;
            ImageProcessor.ResizeImage(applying, geometry);
            image.Composite(applying, CompositeOperator.SrcOver);
            return image;
            }

        ImageProcessor.ResizeImage(image, geometry);
        Debug.WriteLine($"Save aspect ratio - {Settings.SaveAspectRatio}");

        if (Settings.SaveAspectRatio)
            {
            var has_offset = Settings.ResizeMode == ResizeMode.ImageToBounds;
            int x = has_offset ? Settings.Bounds_X : 0;
            int y = has_offset ? Settings.Bounds_Y : 0;

            using var orig = image;
            image = new MagickImage(Settings.PadColor, Width, Height);

            int width = geometry.Width; // must store values direct from geometry
            int height = geometry.Height;

            Debug.WriteLine($"Offset from origin - {x}x{y}");
            Debug.WriteLine($"Image size - {orig.Width}x{orig.Height}");
            Debug.WriteLine($"Pad color - {Settings.PadColor}");
            Debug.WriteLine($"Image position - {Settings.ImagePosition}");

            switch (Settings.ImagePosition)
                {
                case ImagePosition.CenterCloserToOrigin:
                    {
                    x += (width - orig.Width) / 2;
                    y += (height - orig.Height) / 2;
                    break;
                    }
                case ImagePosition.CenterFartherFromOrigin:
                    {
                    var x_diff = width - orig.Width;
                    x += x_diff / 2 + x_diff % 2;
                    var y_diff = height - orig.Height;
                    y += y_diff / 2 + y_diff % 2;
                    break;
                    }
                // case ImagePosition.TopOrLeft: break;  // neither x nor y must be modified
                case ImagePosition.BottomOrRight:
                    {
                    x += width - orig.Width;
                    y += height - orig.Height;
                    break;
                    }
                }

            Debug.WriteLine($"Final offset from origin - {x}x{y}");

            image.Composite(orig, x, y, CompositeOperator.SrcOver);
            image.Composite(ApplyingImage, CompositeOperator.SrcOver);
            }
        else if (Settings.ResizeMode == ResizeMode.ImageToBounds)
            {
            using var orig = image;
            image = ApplyingImage.Clone();

            Debug.WriteLine($"Offset from origin - {Settings.Bounds_X}x{Settings.Bounds_Y}");
            Debug.WriteLine($"Image size - {orig.Width}x{orig.Height}");

            image.Composite(orig, Settings.Bounds_X, Settings.Bounds_Y, CompositeOperator.DstOver);
            }
        else
            image.Composite(ApplyingImage, CompositeOperator.SrcOver);
        return image;
        }

    public void Dispose()
        {
        ApplyingImage.Dispose();
        }
    }
}
