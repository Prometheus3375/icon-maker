using System;
using IconMakerCore.Model.Settings.Appliers;
using ImageMagick;

namespace IconMakerCore.Model.Appliers
{
public interface IApplier: IDisposable
    {
    const MagickFormat ImageFormat = MagickFormat.Png;
    const string FilePattern = "*png";
    static readonly Comparison<IApplier> Compare = (x, y) => String.Compare(x.Name, y.Name, StringComparison.Ordinal);

    string Path { get; }
    string Name { get; }
    int Width { get; }
    int Height { get; }
    ApplierSettings Settings { get; }

    IMagickImage Apply(IMagickImage image);
    }
}
