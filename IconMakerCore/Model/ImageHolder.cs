using ImageMagick;

namespace IconMakerCore.Model
{
public class ImageHolder
    {
    private IMagickImage image;
    public IMagickImage Image
        {
        get => image;
        set
            {
            if (!NoImage)
                image.Dispose();
            image = value;
            }
        }

    public bool NoImage => image is null || image.IsDisposed;
    }
}
