using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using IconMakerCore.Common;
using IconMakerCore.Model.Appliers;
using IconMakerCore.Model.Settings.Appliers;
using ImageMagick;

namespace IconMakerCore.Model
{
public static class ImageProcessor
    {
    public const string FramesIniFile = "frames.ini";

    public const string Path_FramesDirectory = "frames";
    public const string Path_SamplesDirectory = "samples";

    public static string Path_FramesSettings => Path.Combine(Path_FramesDirectory, FramesIniFile);

    public static void Load()
        {
        if (IO.CreateDirectory(Path_FramesDirectory))
            LoadFrames(Path_FramesDirectory);
        if (IO.CreateDirectory(Path_SamplesDirectory))
            LoadSamples(Path_SamplesDirectory);
        }

    //public static void LoadAppliers(string directory, Func<int, string, IApplier> create, List<IApplier> storage)
    //    {
    //    var files = Directory.GetFiles(directory, IApplier.FilePattern);
    //    foreach (var path in files)
    //        {
    //        var border = create(Frames.Count, path);
    //        if (!(border is null))
    //            storage.Add(border);
    //        }
    //    }

    public static void ClearDisposing<T>(this List<T> list) where T: IDisposable
        {
        foreach (var item in list)
            item.Dispose();
        list.Clear();
        }

    public static List<Frame> Frames { get; } = new List<Frame>();
    public static bool HasFrames => Frames.Count > 0;

    public static void SaveFramesSettings()
        {
        if (Frames.Count == 0)
            return;

        var ini = new ApplierIni();
        foreach (var frame in Frames)
            ini.AddSettings(frame);
        ini.Save(Path_FramesSettings);
        }

    public static void LoadFrames(string directory)
        {
        string ini_path = Path_FramesSettings;
        var ini = File.Exists(ini_path) ? new ApplierIni(ini_path) : new ApplierIni();

        var files = Directory.GetFiles(directory, IApplier.FilePattern);
        foreach (var path in files)
            {
            var image = ReadImage(path, IApplier.ImageFormat);
            if (!(image is null))
                {
                var frame = new Frame(path, image);
                frame.Settings = ini.GetSettings(frame);
                Frames.Add(frame);
                }
            }
        Frames.Sort(IApplier.Compare);

        SaveFramesSettings();
        }

    public static void ReloadFrames(string directory)
        {
        Frames.ClearDisposing();
        LoadFrames(directory);
        }

    public static List<Sample> Samples { get; } = new List<Sample>();
    public static bool HasSamples => Samples.Count > 0;

    public static void LoadSamples(string directory)
        {
        var files = Directory.GetFiles(directory, Sample.FilePattern);
        foreach (var path in files)
            {
            var image = ReadImage(path, Sample.ImageFormat);
            if (!(image is null))
                Samples.Add(new Sample(path, image));
            }
        Samples.Sort();
        }

    public static void ReloadSamples(string directory)
        {
        Samples.ClearDisposing();
        LoadSamples(directory);
        }

    public static void ResizeImage(IMagickImage image, MagickGeometry geometry)
        {
        image.Resize(geometry);
        }

    public static IMagickImage ReadImage(string path, MagickFormat format = MagickFormat.Unknown)
        {
        try
            {
            return new MagickImage(path, format);
            }
        catch (MagickException /*e*/)
            {
            // Log error
            return null;
            }
        }

    public static IMagickImage Process(this IMagickImage image, Frame frame = null)
        {
        var clone = image.Clone();
        if (!(frame is null))
            {
            clone = frame.Apply(clone);
            }

        Debug.WriteLine($"Image after frame: {clone.Width}x{clone.Height}");

        return clone;
        }

    public static void SaveToFile(this IMagickImage image, string path)
        {
        image.Write(path);
        }
    }
}
