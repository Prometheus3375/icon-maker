using System;
using IconMakerCore.Common;
using ImageMagick;

namespace IconMakerCore.Model
{
public sealed class Sample: IDisposable, IComparable<Sample>
    {
    public const MagickFormat ImageFormat = MagickFormat.Png;
    public const string FilePattern = "*png";

    public string FilePath { get; private set; }
    public string Identifier { get; private set; }
    public IMagickImage Image { get; private set; }

    public Sample(string path, IMagickImage image)
        {
        FilePath = path;
        Image = image;
        Identifier = image.SizeInfo();
        }

    public void Dispose()
        {
        Image.Dispose();
        }

    public int CompareTo(Sample other)
        {
        int comp = Image.Width.CompareTo(other.Image.Width);
        if (comp != 0)
            return comp;
        comp = Image.Height.CompareTo(other.Image.Height);
        if (comp != 0)
            return comp;
        comp = (Image.Depth * Image.ChannelCount).CompareTo(other.Image.Depth * other.Image.ChannelCount);
        return comp;
        }
    }
}
