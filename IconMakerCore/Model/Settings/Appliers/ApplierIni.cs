using System;
using System.Diagnostics.CodeAnalysis;
using IconMakerCore.Model.Appliers;

namespace IconMakerCore.Model.Settings.Appliers
{
public sealed class ApplierIni: IniDocument
    {
    public enum Version: byte
        {
        First = 1,
        Latest = First
        }

    private static class Key
        {
        public const string ProtocolVersion = "version";
        public const string ResizeMode = "ResizeMode";

        public static class Bounds
            {
            private const string prefix = "Bounds_";
            public const string X = prefix + "X";
            public const string Y = prefix + "Y";
            public const string Width = prefix + "Width";
            public const string Height = prefix + "Height";
            }

        public const string SaveAspectRatio = "SaveAspectRatio";

        public static class PadColor
            {
            private const string prefix = "PadColor_";
            public const string Red = prefix + "Red";
            public const string Green = prefix + "Green";
            public const string Blue = prefix + "Blue";
            public const string Alpha = prefix + "Alpha";
            }

        public const string ImagePosition = "ImagePosition";
        }

    public const string ProtocolSectionName = "protocol";

    private readonly Section protocolSection = new Section(ProtocolSectionName);
    private Version protocol;
    public Version Protocol
        {
        get => protocol;
        private set
            {
            protocol = value;
            protocolSection[Key.ProtocolVersion] = value.ToString("D");
            }
        }

    public ApplierIni()
        {
        Protocol = Version.Latest;
        }

    public ApplierIni(string path)
        {
        Read(path);
        }

    public override string ToString()
        {
        return protocolSection + "\n" + base.ToString();
        }

    public override void Read(string path)
        {
        base.Read(path);
        Protocol = HasSection_(ProtocolSectionName) &&
                   sections[ProtocolSectionName].TryGetValue(Key.ProtocolVersion, out string value) &&
                   Enum.TryParse(value, out Version version) &&
                   Enum.IsDefined(typeof(Version), version)
            ? version
            : Version.Latest;
        RemoveSection_(ProtocolSectionName);
        }

    public void AddSettings(IApplier applier)
        {
        if (Protocol != Version.Latest)
            return;
        // Create new section or clear old
        var name = Section.ConvertName(applier.Name);
        Section section;
        if (HasSection_(name))
            {
            section = sections[name];
            section.Clear();
            }
        else
            section = AddSection(name);

        var settings = applier.Settings;
        #region Resize mode settings
        section.Add(Key.ResizeMode, settings.ResizeMode.ToString("D"));
        section.Add(Key.Bounds.X, settings.Bounds_X.ToString());
        section.Add(Key.Bounds.Y, settings.Bounds_Y.ToString());
        section.Add(Key.Bounds.Width, settings.Bounds_Width.ToString());
        section.Add(Key.Bounds.Height, settings.Bounds_Height.ToString());
        #endregion

        #region Resize options
        section.Add(Key.SaveAspectRatio, settings.SaveAspectRatio.ToString());
        section.Add(Key.ImagePosition, settings.ImagePosition.ToString("D"));
        section.Add(Key.PadColor.Red, settings.PadColor_Red.ToString());
        section.Add(Key.PadColor.Green, settings.PadColor_Green.ToString());
        section.Add(Key.PadColor.Blue, settings.PadColor_Blue.ToString());
        section.Add(Key.PadColor.Alpha, settings.PadColor_Alpha.ToString());
        #endregion
        }

    public ApplierSettings GetSettings(IApplier applier)
        {
        if (!TryGetSection(applier.Name, out Section section))
            return new ApplierSettings(applier);

        var settings = new ApplierSettings(applier);
        return Protocol switch
            {
            _ => GetSettings_Latest(section, settings),
            };
        }

    [SuppressMessage("Style", "IDE0018:Inline variable declaration", Justification = "Same variable used several times")]
    [SuppressMessage("ReSharper", "InlineOutVariableDeclaration", Justification = "Same variable used several times")]
    private static ApplierSettings GetSettings_Latest(Section section, ApplierSettings settings)
        {
        string value;
        #region Resize mode settings
        if (section.TryGetValue(Key.ResizeMode, out value) && Enum.TryParse(value, true, out ResizeMode mode) && Enum.IsDefined(typeof(ResizeMode), mode))
            settings.ResizeMode = mode;

        int x = ApplierSettings.DefaultXY,
            y = ApplierSettings.DefaultXY,
            width = ApplierSettings.AutoSize,
            height = ApplierSettings.AutoSize;

        if (section.TryGetValue(Key.Bounds.X, out value) && !Int32.TryParse(value, out x))
            x = ApplierSettings.DefaultXY;
        if (section.TryGetValue(Key.Bounds.Y, out value) && !Int32.TryParse(value, out y))
            y = ApplierSettings.DefaultXY;
        if (section.TryGetValue(Key.Bounds.Width, out value) && !Int32.TryParse(value, out width))
            width = ApplierSettings.AutoSize;
        if (section.TryGetValue(Key.Bounds.Height, out value) && !Int32.TryParse(value, out height))
            height = ApplierSettings.AutoSize;
        settings.SetResizeBounds(x, y, width, height);
        #endregion
        #region Resize options
        if (section.TryGetValue(Key.SaveAspectRatio, out value) && Boolean.TryParse(value, out bool saveAR))
            settings.SaveAspectRatio = saveAR;
        if (section.TryGetValue(Key.ImagePosition, out value) && Enum.TryParse(value, out ImagePosition pos) && Enum.IsDefined(typeof(ImagePosition), pos))
            settings.ImagePosition = pos;
        byte color;
        if (section.TryGetValue(Key.PadColor.Red, out value) && Byte.TryParse(value, out color))
            settings.PadColor_Red = color;
        if (section.TryGetValue(Key.PadColor.Green, out value) && Byte.TryParse(value, out color))
            settings.PadColor_Green = color;
        if (section.TryGetValue(Key.PadColor.Blue, out value) && Byte.TryParse(value, out color))
            settings.PadColor_Blue = color;
        if (section.TryGetValue(Key.PadColor.Alpha, out value) && Byte.TryParse(value, out color))
            settings.PadColor_Alpha = color;
        #endregion
        return settings;
        }
    }
}
