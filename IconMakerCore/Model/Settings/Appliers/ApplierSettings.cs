using System;
using System.Diagnostics.CodeAnalysis;
using IconMakerCore.Model.Appliers;
using ImageMagick;

namespace IconMakerCore.Model.Settings.Appliers
{
public class ApplierSettings
    {
    public struct Point: IEquatable<Point>
        {
        public int X;
        public int Y;

        public Point(int x, int y)
            {
            X = x;
            Y = y;
            }

        public Point(int xy): this(xy, xy) { }

        public void Update(int x, int y)
            {
            X = x;
            Y = y;
            }

        public void Update(int xy) => Update(xy, xy);
        public void UpdateX(int x) => X = x;
        public void UpdateY(int y) => Y = y;
        public void Reset() => Update(0, 0);
        public void ResetX() => X = 0;
        public void ResetY() => Y = 0;

        public override bool Equals(object obj)
            {
            return obj is Point point && this == point;
            }

        public bool Equals(Point other)
            {
            return this == other;
            }

        [SuppressMessage("ReSharper", "NonReadonlyMemberInGetHashCode")]
        public override int GetHashCode()
            {
            return X * X + Y * Y;
            }

        public override string ToString() => $"({X}; {Y})";

        public static bool operator ==(Point left, Point right) => left.X == right.X && left.Y == right.Y;

        public static bool operator !=(Point left, Point right) => left.X != right.X || left.Y != right.Y;

        public static Point operator +(Point left, Point right) => new Point(left.X + right.X, left.Y + right.Y);

        public static Point operator -(Point left, Point right) => new Point(left.X - right.X, left.Y - right.Y);

        public static Point operator -(Point p) => new Point(-p.X, -p.Y);
        }

    /*public struct RGBA: IEquatable<RGBA>
        {
        public byte Red;
        public byte Green;
        public byte Blue;
        public byte Alpha;

        public RGBA(byte red, byte green, byte blue, byte alpha)
            {
            Red = red;
            Blue = blue;
            Green = green;
            Alpha = alpha;
            }
        public RGBA(byte red, byte green, byte blue) : this(red, green, blue, 0) { }
        public RGBA(byte rgb, byte alpha) : this(rgb, rgb, rgb, alpha) { }
        public RGBA(byte rgb) : this(rgb, rgb, rgb, 0) { }


        public override bool Equals(object obj)
            {
            return obj is RGBA ? this == ((RGBA) obj) : false;
            }

        public bool Equals(RGBA other)
            {
            return this == other;
            }

        public override int GetHashCode()
            {
            return Red * Red + Green * Green + Blue * Blue + Alpha * Alpha;
            }

        public static bool operator ==(RGBA left, RGBA right)
            {
            return left.Red == right.Red &&
                   left.Green == right.Green &&
                   left.Blue == right.Blue &&
                   left.Alpha == right.Alpha;
            }

        public static bool operator !=(RGBA left, RGBA right)
            {
            return left.Red != right.Red ||
                   left.Green != right.Green ||
                   left.Blue != right.Blue ||
                   left.Alpha != right.Alpha;
            }
        }*/

    public static class DefaultPadColor
        {
        public const byte Red = 255;
        public const byte Green = 255;
        public const byte Blue = 255;
        public const byte Alpha = 0;
        //public static RGBA GetRGBA() => new RGBA(Red, Green, Blue, Alpha);
        }

    public const int DefaultXY = 0;
    public const int AutoSize = -1; // must be less than zero
    public const bool DefaultSaveAspectRatio = true;

    public readonly IApplier Applier;
    public readonly MagickGeometry ResizeGeometry;
    protected Point ResizedImageOffset; // must NOT be readonly, if readonly its value is freezed and cannot be changed even via methods
    protected Point ResizedImageSize; // used to memorize w/h for ImageToBounds mode
    public readonly MagickColor PadColor;

    public ApplierSettings(IApplier applier)
        {
        Applier = applier;
        ResizeGeometry = new MagickGeometry(applier.Width, applier.Height);
        ResizedImageOffset = new Point(DefaultXY);
        ResizedImageSize = new Point(applier.Width, applier.Height);
        PadColor = new MagickColor(DefaultPadColor.Red, DefaultPadColor.Green, DefaultPadColor.Blue, DefaultPadColor.Alpha);
        ResetResizeGeometry();
        }

    public void SetResizeBounds(int x, int y, int width, int height)
        {
        if (x < 0 || x > Applier.Width)
            x = 0;
        if (y < 0 || y > Applier.Height)
            y = 0;
        if (width <= 0 || width + x > Applier.Width)
            width = Applier.Width - x;
        if (height <= 0 || height + y > Applier.Height)
            height = Applier.Height - y;

        ResizedImageOffset.Update(x, y);
        ResizedImageSize.Update(width, height);

        if (ResizeMode == ResizeMode.ImageToBounds)
            {
            ResizeGeometry.Width = width;
            ResizeGeometry.Height = height;
            }
        }

    protected void ResetResizeGeometry()
        {
        switch (resizeMode)
            {
            case ResizeMode.ApplierToImage:
                {
                ResizeGeometry.IgnoreAspectRatio = true;
                break;
                }
            case ResizeMode.ImageToApplier:
                {
                ResizeGeometry.Width = Applier.Width;
                ResizeGeometry.Height = Applier.Height;
                ResizeGeometry.IgnoreAspectRatio = !saveAspectRatio;
                break;
                }
            case ResizeMode.ImageToBounds:
                {
                ResizeGeometry.Width = Bounds_Width;
                ResizeGeometry.Height = Bounds_Height;
                ResizeGeometry.IgnoreAspectRatio = !saveAspectRatio;
                break;
                }
            }
        }

    protected ResizeMode resizeMode = ResizeMode.Default;
    public ResizeMode ResizeMode
        {
        get => resizeMode;
        set
            {
            var old = resizeMode;
            resizeMode = value;
            if (value != old)
                ResetResizeGeometry();
            }
        }
    public int Bounds_X
        {
        get => ResizedImageOffset.X;
        set => SetResizeBounds(value, Bounds_Y, Bounds_Width, Bounds_Height);
        }
    public int Bounds_Y
        {
        get => ResizedImageOffset.Y;
        set => SetResizeBounds(Bounds_X, value, Bounds_Width, Bounds_Height);
        }
    public int Bounds_Width
        {
        get => ResizedImageSize.X;
        set => SetResizeBounds(Bounds_X, Bounds_Y, value, Bounds_Height);
        }
    public int Bounds_Height
        {
        get => ResizedImageSize.Y;
        set => SetResizeBounds(Bounds_X, Bounds_Y, Bounds_Width, value);
        }

    protected bool saveAspectRatio = DefaultSaveAspectRatio;
    public bool SaveAspectRatio
        {
        get => saveAspectRatio;
        set
            {
            saveAspectRatio = value;
            if (resizeMode != ResizeMode.ApplierToImage)
                ResizeGeometry.IgnoreAspectRatio = !value;
            }
        }

    public byte PadColor_Red
        {
        get => PadColor.R;
        set => PadColor.R = value;
        }
    public byte PadColor_Green
        {
        get => PadColor.G;
        set => PadColor.G = value;
        }
    public byte PadColor_Blue
        {
        get => PadColor.B;
        set => PadColor.B = value;
        }
    public byte PadColor_Alpha
        {
        get => PadColor.A;
        set => PadColor.A = value;
        }

    public ImagePosition ImagePosition { get; set; } = ImagePosition.Default;
    }
}
