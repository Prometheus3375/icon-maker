namespace IconMakerCore.Model.Settings.Appliers
{
public enum ImagePosition: byte
    {
    CenterCloserToOrigin = 1,
    CenterFartherFromOrigin,
    TopOrLeft,
    BottomOrRight,
    Default = CenterCloserToOrigin
    }

public static class ImagePositionExt
    {
    public static string GetLocalizedString(this ImagePosition pos)
        {
        return pos switch
            {
            ImagePosition.CenterCloserToOrigin => "Center ↖",
            ImagePosition.CenterFartherFromOrigin => "Center ↘",
            ImagePosition.TopOrLeft => "Top/Left",
            ImagePosition.BottomOrRight => "Bottom/Right",
            _ => "Undefined",
            };
        }

    public static ImagePosition FromLocalizedString(string pos)
        {
        return pos switch
            {
            "Center ↖" => ImagePosition.CenterCloserToOrigin,
            "Center ↘" => ImagePosition.CenterFartherFromOrigin,
            "Top/Left" => ImagePosition.TopOrLeft,
            "Bottom/Right" => ImagePosition.BottomOrRight,
            _ => ImagePosition.Default,
            };
        }
    }
}
