namespace IconMakerCore.Model.Settings.Appliers
{
public enum ResizeMode: byte
    {
    ApplierToImage = 1,
    ImageToApplier,
    ImageToBounds,
    Default = ImageToApplier
    }
}
