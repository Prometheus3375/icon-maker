using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace IconMakerCore.Model.Settings
{
public class IniDocument
    {
    public sealed class Section: IComparable<Section>
        {
        public const char KeyValueSeparator = '=';

        private const string NameRegexString = @"[\w -~]+";
        public static readonly Regex NameRegex = new Regex($@"^{NameRegexString}$", RegexOptions.Compiled | RegexOptions.Singleline);
        public static readonly Regex StartRegex = new Regex($@"^\[{NameRegexString}\]$", RegexOptions.Compiled | RegexOptions.Singleline);
        public static readonly Regex KeyRegex = new Regex(@"^\w+$", RegexOptions.Compiled | RegexOptions.Singleline);
        public static readonly Regex ValueRegex = new Regex(@"^[\w -~]*$", RegexOptions.Compiled | RegexOptions.Singleline);

        public static bool IsProperName(string name) => NameRegex.IsMatch(name);
        public static bool IsProperKey(string key) => KeyRegex.IsMatch(key);
        public static bool IsProperValue(string value) => ValueRegex.IsMatch(value);

        public static string ConvertName(string name) => name.Trim();
        public static string ConvertKey(string key) => key.Trim();
        public static string ConvertValue(string value) => value.Trim();

        private readonly Dictionary<string, string> dict = new Dictionary<string, string>();
        public string Name { get; }

        public Section(string name)
            {
            name = ConvertName(name);
            if (!IsProperName(name))
                throw new ArgumentException("Name does not match pattern", nameof(name));
            Name = name;
            }

        private static string ConvertCheckKey(string key)
            {
            key = ConvertKey(key);
            if (!IsProperKey(key))
                throw new ArgumentException("Key does not match pattern", nameof(key));
            return key;
            }

        private static string ConvertCheckValue(string value)
            {
            value = ConvertValue(value);
            if (!IsProperValue(value))
                throw new ArgumentException("Value does not match pattern", nameof(value));
            return value;
            }

        public string this[string key]
            {
            get => dict[ConvertKey(key)];
            set => dict[ConvertCheckKey(key)] = ConvertCheckValue(value);
            }

        public bool TryGetValue(string key, out string value) => dict.TryGetValue(ConvertKey(key), out value);

        public bool ContainsKey(string key) => dict.ContainsKey(ConvertKey(key));
        public bool ContainsValue(string value) => dict.ContainsValue(ConvertValue(value));

        public void Add(string key, string value = "") => dict.Add(ConvertCheckKey(key), ConvertCheckValue(value));

        public bool TryAdd(string key, string value = "")
            {
            key = ConvertKey(key);
            if (dict.ContainsKey(key))
                return false;
            if (!IsProperKey(key))
                return false;
            value = ConvertValue(value);
            if (!IsProperValue(value))
                return false;

            dict.Add(key, value);
            return true;
            }

        public bool Remove(string key) => dict.Remove(ConvertKey(key));
        public bool Remove(string key, out string value) => dict.Remove(ConvertKey(key), out value);

        public void Clear() => dict.Clear();

        public override string ToString()
            {
            var strings = new List<string> {$"[{Name}]"};
            foreach (var pair in dict)
                {
                if (String.IsNullOrEmpty(pair.Value))
                    strings.Add($"{pair.Key}");
                else
                    strings.Add($"{pair.Key}{KeyValueSeparator}{pair.Value}");
                }
            return String.Join("\n", strings) + "\n";
            }

        public int CompareTo(Section other)
            {
            return String.Compare(Name, other.Name, StringComparison.Ordinal);
            }
        }

    public static readonly Encoding FileEncoding = new UTF8Encoding(false, false);

    public readonly Dictionary<string, Section> sections = new Dictionary<string, Section>();

    public IniDocument() { }

    [SuppressMessage("ReSharper", "VirtualMemberCallInConstructor")]
    public IniDocument(string path)
        {
        Read(path);
        }

    public Section this[string section] => sections[Section.ConvertName(section)];

    public string this[string section, string key]
        {
        get => this[section][key];
        set
            {
            section = Section.ConvertName(section);
            if (!HasSection_(section))
                AddSection(section);
            sections[section][key] = value;
            }
        }

    protected bool HasSection_(string name) => sections.ContainsKey(name);
    public bool HasSection(string name) => HasSection_(Section.ConvertName(name));

    public Section AddSection(string name)
        {
        var s = new Section(name);
        sections.Add(name, s);
        return s;
        }

    public bool TryAddSection(string name, out Section section)
        {
        name = Section.ConvertName(name);
        if (HasSection_(name))
            {
            section = sections[name];
            return false;
            }
        if (!Section.IsProperName(name))
            {
            section = null;
            return false;
            }

        section = AddSection(name);
        return true;
        }

    public bool TryGetSection(string name, out Section section) => sections.TryGetValue(Section.ConvertName(name), out section);

    protected bool RemoveSection_(string name) => sections.Remove(name);
    public bool RemoveSection(string name) => RemoveSection_(Section.ConvertName(name));
    protected bool RemoveSection_(string name, out Section section) => sections.Remove(name, out section);
    public bool RemoveSection(string name, out Section section) => RemoveSection_(Section.ConvertName(name), out section);

    public bool HasKey_(string section, string key) => sections[section].ContainsKey(key);
    public bool HasKey(string section, string key) => HasKey_(Section.ConvertName(section), key);

    public override string ToString()
        {
        var vals = sections.Values.ToList();
        vals.Sort();
        return String.Join("\n", vals);
        }

    protected void ParseStrings(string[] strings)
        {
        Section section = null;
        foreach (var s in strings)
            {
            var line = s.Trim();
            if (line == String.Empty)
                continue;

            if (Section.StartRegex.IsMatch(line))
                {
                // This line is section header
                TryAddSection(line[1..^1], out section);
                }
            else if (!(section is null))
                {
                if (line[0] == ';' || line[0] == '#')
                    // This line is comment
                    continue;
                int sep_pos = line.IndexOf(Section.KeyValueSeparator);
                if (sep_pos < 0)
                    {
                    // No value
                    section.TryAdd(line);
                    }
                else
                    {
                    // Has value
                    section.TryAdd(line[..sep_pos], line[(sep_pos + 1)..]);
                    }
                }
            }
        }

    public virtual void Read(string path)
        {
        sections.Clear();
        ParseStrings(File.ReadAllLines(path, FileEncoding));
        }

    public void Save(string path)
        {
        File.WriteAllText(path, ToString(), FileEncoding);
        }
    }
}
