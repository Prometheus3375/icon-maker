using System;
using System.Windows.Input;

namespace IconMakerCore.Services
{
public interface ICommandFactory
    {
    private static ICommandFactory factory;
    public static ICommandFactory Factory
        {
        get => factory;
        set => factory ??= value;
        }

    public static ICommand Create(Action<object> execute, Func<object, bool> canExecute = null) => Factory.New(execute, canExecute);

    ICommand New(Action<object> execute, Func<object, bool> canExecute = null);
    }
}
