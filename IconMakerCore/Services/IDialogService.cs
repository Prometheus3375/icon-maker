namespace IconMakerCore.Services
{
public interface IDialogService
    {
    FileDialogOutput OpenFileDialog();
    FileDialogOutput SaveFileDialog(string suggestedName = "");
    void MessageBox(string message);
    }

public class FileDialogOutput
    {
    public bool Accepted { get; }
    public string Path { get; }
    public string[] Paths { get; }

    public FileDialogOutput(bool accepted, string path = null, string[] paths = null)
        {
        Accepted = accepted;
        Path = path;
        Paths = paths;
        }
    }
}
