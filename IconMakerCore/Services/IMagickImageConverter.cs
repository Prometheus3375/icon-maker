using System;
using ImageMagick;

namespace IconMakerCore.Services
{
public interface IMagickImageConverter
    {
    private static IMagickImageConverter converter;
    public static IMagickImageConverter Converter
        {
        get => converter;
        set => converter ??= value;
        }

    public static object Convert(IMagickImage image) => converter.ConvertImage(image);

    object ConvertImage(IMagickImage image);
    }
}
