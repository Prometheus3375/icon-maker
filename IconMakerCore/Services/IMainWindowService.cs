namespace IconMakerCore.Services
{
public interface IMainWindowService: IDialogService
    {
    void ToggleFramesWindow();
    bool? OpenSettings();
    }
}
