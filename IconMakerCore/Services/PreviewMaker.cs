using System.Diagnostics;
using ImageMagick;

namespace IconMakerCore.Services
{
public sealed class PreviewMaker
    {
    private readonly MagickGeometry previewGeometry;

    public PreviewMaker(int previewSize)
        {
        previewGeometry = new MagickGeometry(previewSize)
            {
            FillArea = false, // set false to resize using bigger dimension, ex. (30x5 -> 300x300) = 300x50, true - smaller, ex. (30x5 -> 300x300) = 1800x300
            IgnoreAspectRatio = false, // set false to save w/h ratio.
            Greater = true, // set false to resize if geometry > image
            Less = false // set false to resize if geometry < image
            };
        }

    public object MakePreview(IMagickImage source)
        {
        using var image = source.Clone();
        image.Scale(previewGeometry);

        Debug.WriteLine($"Image after scaling: {image.Width}x{image.Height}");

        return IMagickImageConverter.Convert(image);
        }
    }
}
