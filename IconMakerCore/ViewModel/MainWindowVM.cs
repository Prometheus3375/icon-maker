using System.Windows.Input;
using IconMakerCore.Common;
using IconMakerCore.Model;
using IconMakerCore.Model.Appliers;
using IconMakerCore.Services;

namespace IconMakerCore.ViewModel
{
public class MainWindowVM: ViewModelBase
    {
    #region Preview properties
    private object previewSource;
    public object PreviewSource
        {
        get => previewSource;
        set
            {
            previewSource = value;
            OnPropertyChanged();
            }
        }
    #endregion
    #region Image info properties
    private string resultSizeInfo;
    public string ResultSizeInfo
        {
        get => resultSizeInfo;
        set
            {
            if (HasModifiedSource)
                {
                resultSizeInfo = value;
                OnPropertyChanged();
                }
            else
                resultSizeInfo = null;
            }
        }
    private string sourceSizeInfo;
    public string SourceSizeInfo
        {
        get => sourceSizeInfo;
        set
            {
            if (HasSource)
                {
                sourceSizeInfo = value;
                OnPropertyChanged();
                }
            else
                sourceSizeInfo = null;
            }
        }
    #endregion
    #region Current frame properties
    private Frame currentFrame;
    public Frame CurrentFrame
        {
        get => currentFrame;
        set
            {
            currentFrame = value;
            OnPropertyChanged(nameof(CurrentFrameView));
            }
        }
    public Frame CurrentFrameView
        {
        get => currentFrame;
        set
            {
            currentFrame = value;
            UpdateImageView();
            }
        }
    #endregion
    #region Source properties
    private bool noSource = true;
    public bool HasSource => !noSource;
    public bool NoSource
        {
        get => noSource;
        set
            {
            noSource = value;
            OnPropertyChanged();
            OnPropertyChanged(nameof(HasSource));
            }
        }
    #endregion

    public bool IsSourceUnmodified => currentFrame is null;
    public bool HasModifiedSource => !(noSource || IsSourceUnmodified);

    private readonly PreviewMaker PreviewMaker;
    private readonly ImageHolder Source;
    private readonly ImageHolder Result;

    public ICommand Command_ReadImage { get; }
    public ICommand Command_SaveImage { get; }
    public ICommand Command_ViewFrames { get; }
    public ICommand Command_ResetImage { get; }
    public ICommand Command_OpenSettings { get; }

    public void UpdateImageView()
        {
        OnPropertyChanged(nameof(HasModifiedSource));
        Result.Image = Source.Image.Process(CurrentFrame);
        PreviewSource = PreviewMaker.MakePreview(Result.Image);
        ResultSizeInfo = $"{Result.Image.SizeInfo()}";
        }

    public void ResetImage()
        {
        CurrentFrame = null;
        }

    public MainWindowVM(IMainWindowService service, int previewSize)
        {
        PreviewMaker = new PreviewMaker(previewSize);
        Source = new ImageHolder();
        Result = new ImageHolder();

        Command_ReadImage = ICommandFactory.Create(
            obj =>
                {
                var result = service.OpenFileDialog();
                if (result.Accepted)
                    {
                    Source.Image = ImageProcessor.ReadImage(result.Path);
                    if (NoSource)
                        NoSource = false;
                    SourceSizeInfo = $"Source image size - {Source.Image.SizeInfo()}";
                    ResetImage();
                    UpdateImageView();
                    }
                }
            );

        Command_SaveImage = ICommandFactory.Create(
            obj =>
                {
                var result = service.SaveFileDialog();
                if (result.Accepted)
                    {
                    Result.Image.SaveToFile(result.Path);
                    }
                },
            obj => HasSource
            );

        Command_ViewFrames = ICommandFactory.Create(
            obj =>
                {
                service.ToggleFramesWindow();
                },
            obj => HasSource && ImageProcessor.HasFrames
            );

        Command_ResetImage = ICommandFactory.Create(
            obj =>
                {
                ResetImage();
                UpdateImageView();
                },
            obj => HasModifiedSource
            );

        Command_OpenSettings = ICommandFactory.Create(
            obj =>
                {
                service.OpenSettings();
                },
            obj => ImageProcessor.HasFrames
            );
        }

    /// <summary>
    ///     Constructor only for designer
    /// </summary>
    public MainWindowVM() { }
    }
}
