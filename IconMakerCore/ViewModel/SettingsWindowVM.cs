using System.Windows.Input;
using IconMakerCore.Common;
using IconMakerCore.Model;
using IconMakerCore.Model.Appliers;
using IconMakerCore.Model.Settings.Appliers;
using IconMakerCore.Services;

namespace IconMakerCore.ViewModel
{
public sealed class SettingsWindowVM: ViewModelBase
    {
    #region Frames
    private Frame selectedFrame;
    public Frame SelectedFrame
        {
        get => selectedFrame;
        set
            {
            selectedFrame = value;
            UpdatePreview();
            OnPropertyChanged(nameof(ResizeMode));
            UpdateHorizontalBounds();
            UpdateVerticalBounds();
            OnPropertyChanged(nameof(Bounds_MaxX));
            OnPropertyChanged(nameof(Bounds_MaxY));
            OnPropertyChanged(nameof(SaveAspectRatio));
            ResetPadColorView();
            OnPropertyChanged(nameof(PadColor));
            OnPropertyChanged(nameof(Position));
            }
        }
    #endregion
    #region Resize mode
    public ResizeMode ResizeMode
        {
        get => SelectedFrame.Settings.ResizeMode;
        set
            {
            SelectedFrame.Settings.ResizeMode = value;
            UpdatePreview();
            }
        }
    #region Bounds
    public int Bounds_X
        {
        get => SelectedFrame.Settings.Bounds_X;
        set
            {
            SelectedFrame.Settings.Bounds_X = value;
            UpdatePreview();
            UpdateHorizontalBounds();
            }
        }
    public int Bounds_Y
        {
        get => SelectedFrame.Settings.Bounds_Y;
        set
            {
            SelectedFrame.Settings.Bounds_Y = value;
            UpdatePreview();
            UpdateVerticalBounds();
            }
        }
    public int Bounds_Width
        {
        get => SelectedFrame.Settings.Bounds_Width;
        set
            {
            SelectedFrame.Settings.Bounds_Width = value;
            UpdatePreview();
            }
        }
    public int Bounds_Height
        {
        get => SelectedFrame.Settings.Bounds_Height;
        set
            {
            SelectedFrame.Settings.Bounds_Height = value;
            UpdatePreview();
            }
        }
    #endregion
    #region MaxBounds
    public int Bounds_MaxX => SelectedFrame.Width;
    public int Bounds_MaxY => SelectedFrame.Height;
    public int Bounds_MaxWidth => Bounds_MaxX - Bounds_X;
    public int Bounds_MaxHeight => Bounds_MaxY - Bounds_Y;
    #endregion
    #endregion

    private void UpdateHorizontalBounds()
        {
        OnPropertyChanged(nameof(Bounds_X));
        OnPropertyChanged(nameof(Bounds_Width));
        OnPropertyChanged(nameof(Bounds_MaxWidth));
        }

    private void UpdateVerticalBounds()
        {
        OnPropertyChanged(nameof(Bounds_Y));
        OnPropertyChanged(nameof(Bounds_Height));
        OnPropertyChanged(nameof(Bounds_MaxHeight));
        }

    #region Resize options
    public bool SaveAspectRatio
        {
        get => SelectedFrame.Settings.SaveAspectRatio;
        set
            {
            SelectedFrame.Settings.SaveAspectRatio = value;
            UpdatePreview();
            }
        }
    #region Save aspect ratio options
    private string padColorInput;
    public string PadColor
        {
        get => padColorInput;
        set
            {
            padColorInput = value;
            if (ColorParser.TryParse(value, SelectedFrame.Settings.PadColor))
                UpdatePreview();
            }
        }
    public ImagePosition Position
        {
        get => SelectedFrame.Settings.ImagePosition;
        set
            {
            SelectedFrame.Settings.ImagePosition = value;
            UpdatePreview();
            }
        }
    #endregion
    #endregion

    public void ResetPadColorView()
        {
        padColorInput = SelectedFrame.Settings.PadColor.ToARGBString();
        OnPropertyChanged(nameof(PadColor));
        }

    #region Preview
    private object previewSource;
    public object PreviewSource
        {
        get => previewSource;
        set
            {
            previewSource = value;
            OnPropertyChanged();
            }
        }
    private string previewSizeInfo;
    public string PreviewSizeInfo
        {
        get => previewSizeInfo;
        set
            {
            previewSizeInfo = value;
            OnPropertyChanged();
            }
        }
    private Sample selectedSample;
    public Sample SelectedSample
        {
        get => selectedSample;
        set
            {
            selectedSample = value;
            UpdatePreview();
            }
        }
    #endregion

    private readonly ImageHolder Result;
    private readonly PreviewMaker PreviewMaker;

    private void UpdatePreview()
        {
        if (ImageProcessor.HasSamples)
            {
            Result.Image = SelectedSample.Image.Process(SelectedFrame);
            PreviewSource = PreviewMaker.MakePreview(Result.Image);
            PreviewSizeInfo = Result.Image.SizeInfo();
            }
        }

    public ICommand Command_PadColorUpdate { get; }
    public ICommand Command_SaveSettings { get; }

    public SettingsWindowVM(int previewSize)
        {
        Result = new ImageHolder();
        PreviewMaker = new PreviewMaker(previewSize);

        Command_PadColorUpdate = ICommandFactory.Create(obj => ResetPadColorView());
        Command_SaveSettings = ICommandFactory.Create(obj => ImageProcessor.SaveFramesSettings());

        if (ImageProcessor.HasSamples)
            selectedSample = ImageProcessor.Samples[0];

        if (ImageProcessor.HasFrames)
            // This will update preview
            SelectedFrame = ImageProcessor.Frames[0];
        }

    /// <summary>
    ///     Constructor only for designer
    /// </summary>
    public SettingsWindowVM() { }
    }
}
