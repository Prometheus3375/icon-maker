using System.Collections.Generic;
using IconMakerCore.Common;
using IconMakerCore.Model;
using IconMakerCore.Model.Appliers;
using IconMakerCore.Model.Settings.Appliers;

namespace IconMakerCore.ViewModel
{
public static class StaticFields
    {
    #region Common
    public static List<Frame> Frames => ImageProcessor.Frames;
    public static List<Sample> Samples => ImageProcessor.Samples;

    public static bool ThereAreFrames => ImageProcessor.HasFrames;
    public static bool ThereAreSamples => ImageProcessor.HasSamples;
    #endregion
    #region Settings
    public static ResizeMode ApplierToImage => ResizeMode.ApplierToImage;
    public static ResizeMode ImageToApplier => ResizeMode.ImageToApplier;
    public static ResizeMode ImageToBounds => ResizeMode.ImageToBounds;

    public static readonly ImagePosition[] ImagePositions = Extensions.EnumGetUniqueValues<ImagePosition>();
    #endregion
    }
}
