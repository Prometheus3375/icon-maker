using System.ComponentModel;
using System.Diagnostics;
using System.Runtime.CompilerServices;

namespace IconMakerCore.ViewModel
{
public abstract class ViewModelBase: INotifyPropertyChanged
    {
    public event PropertyChangedEventHandler PropertyChanged;

    public void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
        Debug.WriteLine($"OnPropertyChanged of {this} received \"{propertyName}\" property");

        PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
    }
}
