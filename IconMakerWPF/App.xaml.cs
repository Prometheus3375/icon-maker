using System.IO;
using System.Windows;
using IconMakerCore.Model;
using IconMakerCore.Services;
using IconMakerWPF.Services;
using ImageMagick;

namespace IconMakerWPF
{
/// <summary>
///     Interaction logic for App.xaml
/// </summary>
public partial class App: Application
    {
    protected override void OnStartup(StartupEventArgs e)
        {
#if TEST
        var cwd = new DirectoryInfo(Directory.GetCurrentDirectory());
        // ReSharper disable PossibleNullReferenceException
        for (int i = 0; i < 4; ++i)
            cwd = cwd.Parent;
        Directory.SetCurrentDirectory(cwd.FullName);
        // ReSharper restore PossibleNullReferenceException
#endif
        ImageProcessor.Load();
        // Disable OpenCL
        OpenCL.IsEnabled = false;
        // Create factory that will create commands for WPF
        ICommandFactory.Factory = new CommandFactory();
        // Create image converter that will convert Magick.NET images to UI images
        IMagickImageConverter.Converter = new MagickImageConverter();
        // Run base method
        base.OnStartup(e);
        }
    }
}
