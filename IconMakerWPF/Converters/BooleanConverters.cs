using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;

namespace IconMakerWPF.Converters
{
public class BooleanAndConverter: IMultiValueConverter
    {
    public object Convert(object[] values = null, Type targetType = null, object parameter = null, CultureInfo culture = null)
        {
        if (values is null)
            return false;
        foreach (var v in values)
            {
            if (!(v is bool))
                return DependencyProperty.UnsetValue;
            if (!(bool) v)
                return false;
            }
        return true;
        }

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter = null, CultureInfo culture = null)
        {
        var result = new object[targetTypes.Length];
        if (value is bool b && b)
            {
            for (int i = 0; i < result.Length; i++)
                result[i] = true;
            }
        else
            {
            for (int i = 0; i < result.Length; i++)
                result[i] = DependencyProperty.UnsetValue;
            }
        return result;
        }
    }

public class InverseBooleanConverter: IValueConverter
    {
    public object Convert(object value, Type targetType = null, object parameter = null, CultureInfo culture = null)
        {
        return value is bool b ? !b : DependencyProperty.UnsetValue;
        }

    public object ConvertBack(object value, Type targetType = null, object parameter = null, CultureInfo culture = null)
        {
        return value is bool b ? !b : DependencyProperty.UnsetValue;
        }
    }
}
