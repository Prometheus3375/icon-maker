using System;
using System.Globalization;
using System.Windows.Controls;
using System.Windows.Data;

namespace IconMakerWPF.Converters
{
public class BooleanAndToVisibilityConverter: IMultiValueConverter
    {
    protected readonly BooleanToVisibilityConverter b2v = new BooleanToVisibilityConverter();
    protected readonly BooleanAndConverter bAnd = new BooleanAndConverter();

    public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
        var result = bAnd.Convert(values);
        return b2v.Convert(result, null, null, null);
        }

    public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
        var result = b2v.ConvertBack(value, null, null, null);
        return bAnd.ConvertBack(result, targetTypes);
        }
    }
}
