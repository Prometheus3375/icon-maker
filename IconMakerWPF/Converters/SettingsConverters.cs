using System;
using System.Globalization;
using System.Windows;
using System.Windows.Data;
using IconMakerCore.Model.Settings.Appliers;
using ResizeMode = IconMakerCore.Model.Settings.Appliers.ResizeMode;

namespace IconMakerWPF.Converters
{
public class ResizeModeToBooleanConverter: IValueConverter
    {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
        //Debug.WriteLine($"{nameof(ResizeModeToBooleanConverter)}.Convert: value = {value} of {value.GetType()}, parameter = {parameter} of {parameter.GetType()}");
        return value is ResizeMode val && parameter is ResizeMode par
            ? par == val
            : DependencyProperty.UnsetValue;
        }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
        //Debug.WriteLine($"{nameof(ResizeModeToBooleanConverter)}.ConvertBack: value = {value} of {value.GetType()}, parameter = {parameter} of {parameter.GetType()}");
        return value is bool b && b && parameter is ResizeMode mode
            ? mode
            : Binding.DoNothing;
        }
    }

public class ImagePositionToStringConverter: IValueConverter
    {
    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
        return value is ImagePosition pos ? pos.GetLocalizedString() : DependencyProperty.UnsetValue;
        }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
        return value is string s ? ImagePositionExt.FromLocalizedString(s) : Binding.DoNothing;
        }
    }
}
