using System;
using System.Windows.Input;
using IconMakerCore.Services;

namespace IconMakerWPF.Services
{
public class CommandFactory: ICommandFactory
    {
    public ICommand New(Action<object> execute, Func<object, bool> canExecute = null)
        {
        return new Command(execute, canExecute);
        }
    }
}
