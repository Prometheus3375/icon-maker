using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using IconMakerCore.Services;
using ImageMagick;

namespace IconMakerWPF.Services
{
public class MagickImageConverter: IMagickImageConverter
    {
    private const double DefaultDPI = 96; // Default DPI for WPF

    public object ConvertImage(IMagickImage image)
        {
        var pxform = image.HasAlpha ? PixelFormats.Bgra32 : PixelFormats.Bgr24;
        string mapping = image.HasAlpha ? "BGRA" : "BGR";

        using var pixels = image.GetPixelsUnsafe();
        byte[] bytes = pixels.ToByteArray(mapping);
        int stride = bytes.Length / image.Height;
        //var stride = image.Width * (pxform.BitsPerPixel / 8);

#if DEBUG
        var v = BitmapSource.Create(image.Width, image.Height, DefaultDPI, DefaultDPI, pxform, null, bytes, stride);
        Debug.WriteLine($"Image converted: pxsize - {v.PixelWidth}x{v.PixelHeight}; size - {v.Width}x{v.Height}; dpi - {v.DpiX}x{v.DpiY}");
        return v;
#else
        return BitmapSource.Create(image.Width, image.Height, DefaultDPI, DefaultDPI, pxform, null, bytes, stride);
#endif

        // This solution does not preserve alpha
        //using var stream = new MemoryStream();
        //image.Write(stream, MagickFormat.Bmp3);
        //var bitmap = new BitmapImage();
        //bitmap.BeginInit();
        //bitmap.StreamSource = stream;
        //bitmap.CacheOption = BitmapCacheOption.OnLoad;
        //bitmap.EndInit();
        //bitmap.Freeze();

        //return bitmap;
        }
    }
}
