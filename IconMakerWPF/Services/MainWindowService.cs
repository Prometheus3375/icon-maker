using System;
using System.Windows;
using IconMakerCore.Services;
using IconMakerWPF.Windows;
using Microsoft.Win32;

namespace IconMakerWPF.Services
{
public class MainWindowService: IMainWindowService
    {
    public readonly MainWindow HostWindow;

    public MainWindowService(MainWindow window)
        {
        HostWindow = window;
        }

    private FramesWindow Frames { get; set; }

    public void InitAttachedWindows()
        {
        Frames = new FramesWindow(HostWindow);
        }

    public void ToggleFramesWindow()
        {
        if (Frames.IsVisible)
            Frames.Hide();
        else
            Frames.Show();
        }

    public bool? OpenSettings()
        {
        return new SettingsWindow(HostWindow).ShowDialog();
        }

    public FileDialogOutput OpenFileDialog()
        {
        var dialog = new OpenFileDialog
            {
            //Title = title,
            //InitialDirectory = initial_dir,
            //Filter = filter,
            Multiselect = false,
            };
        return new FileDialogOutput(dialog.ShowDialog(HostWindow) == true, path: dialog.FileName);
        }

    public FileDialogOutput SaveFileDialog(string suggestedName = "")
        {
        var dialog = new SaveFileDialog
            {
            FileName = suggestedName,
            //Title = title,
            //InitialDirectory = initial_dir,
            AddExtension = true,
            CheckFileExists = false,
            CreatePrompt = false,
            OverwritePrompt = true,
            //Filter = filter,
            };

        return new FileDialogOutput(dialog.ShowDialog(HostWindow) == true, path: dialog.FileName);
        }

    public void MessageBox(string message)
        {
        throw new NotImplementedException();
        }
    }
}
