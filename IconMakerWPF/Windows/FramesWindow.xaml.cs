using System;
using System.ComponentModel;
using System.Diagnostics;
using System.Windows;

namespace IconMakerWPF.Windows
{
/// <summary>
///     Interaction logic for FramesWindow.xaml
/// </summary>
public partial class FramesWindow: Window
    {
    public FramesWindow(Window owner)
        {
        InitializeComponent();
        Owner = owner;
        DataContext = owner.DataContext;
        }

    private void Window_ContentRendered(object sender, EventArgs e)
        {
        Debug.WriteLine($"Frames window's content is rendered, actual size {ActualWidth}x{ActualHeight}");

        MinWidth = ActualWidth;
        MinHeight = ActualHeight;
        }

    private void Window_Closing(object sender, CancelEventArgs e)
        {
        e.Cancel = true;
        if (IsVisible)
            Hide();
        }
    }
}
