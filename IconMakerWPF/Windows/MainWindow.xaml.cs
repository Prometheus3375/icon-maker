using System;
using System.Diagnostics;
using System.Windows;
using IconMakerCore.ViewModel;
using IconMakerWPF.Services;

namespace IconMakerWPF.Windows
{
/// <summary>
///     Interaction logic for MainWindow.xaml
/// </summary>
public partial class MainWindow: Window
    {
    private const int PreviewSizePx = 400;

    public MainWindow()
        {
        InitializeComponent();
        }

    private void Window_ContentRendered(object sender, EventArgs e)
        {
        Debug.WriteLine("Main window's content is rendered");

        var service = new MainWindowService(this);
        DataContext = new MainWindowVM(service, PreviewSizePx);
        service.InitAttachedWindows();
        }
    }
}
