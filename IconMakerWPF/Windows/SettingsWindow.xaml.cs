using System.Windows;
using IconMakerCore.ViewModel;
using IconMakerWPF.Services;

namespace IconMakerWPF.Windows
{
/// <summary>
///     Interaction logic for Settings.xaml
/// </summary>
public partial class SettingsWindow: Window
    {
    private const int PreviewSizePx = 300;

    public SettingsWindow(Window owner)
        {
        InitializeComponent();
        Owner = owner;
        DataContext = new SettingsWindowVM(PreviewSizePx);
        }
    }
}
